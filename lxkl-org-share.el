;;; Copyright 2023 Lasse Kliemann <lasse@lassekliemann.de>
;;; MIT License
;;; https://gitlab.com/lxkl/org-share

(require 'ox-html)

(defun lxkl-org-share--slurp (fn)
  "Return file content as trimmed string."
  (string-trim (with-temp-buffer (insert-file-contents fn) (buffer-string))))

(defun lxkl-org-share--update-htaccess (dir)
  "Create a file `.htaccess' in directory DIR based on the
`*.require' files found in this directory."
  (with-temp-file
      (format "%s/.htaccess" dir)
    (insert-file-contents (format "%s/config/htaccess.header" dir))
    (goto-char (point-max))
    (insert (mapconcat
             (lambda (fn)
               (let ((name (file-name-sans-extension (file-name-nondirectory fn)))
                     (req (lxkl-org-share--slurp fn)))
                 (format "<Files %s>\n  Require %s\n</Files>" (format "%s.html" name) req)))
             (file-expand-wildcards (format "%s/*.require" dir))
             "\n"))))

(defun lxkl-org-share--property (var &optional default)
  "Return Org property. The argument VAR typically is one of `Dir',
`Name', or `Require'."
  (or (org-entry-get (point) (format "Share%s" var)) default))

(defun lxkl-org-share--config-var (dir var)
  "Check config dir for a file called VAR and return its content if
found. Otherwise, return value bound to symbol VAR, if any."
  (let ((fn (format "%s/config/%s" dir var)))
    (cond
     ((file-exists-p fn) (lxkl-org-share--slurp fn))
     ((boundp var) (symbol-value var))
     (t nil))))

(defun lxkl-org-share--merge-alists (alist1 alist2)
  "Merge ALIST2 into ALIST1 without modifying ALIST1; return merged
alist."
  (let ((res (copy-alist alist1)))
    (dolist (e alist2 res)
      (cond
       ((assoc (car e) res) (setcdr (assoc (car e) res) (cdr e)))
       (t (push e res))))))

(defun lxkl-org-share--config-alist (dir pat var)
  "Expand pattern PAT in config dir and construct an alist from
it. Merge this alist into alist designated by symbol VAR, using
an empty list if not bound."
  (lxkl-org-share--merge-alists
   (if (boundp var) (symbol-value var) nil)
   (mapcar
    (lambda (fn) (list (file-name-extension fn) (lxkl-org-share--slurp fn)))
    (file-expand-wildcards (format "%s/config/%s" dir pat)))))

(defmacro with-values-restored (vars &rest body)
  "Symbols in VARS will be set to their original values after body
is done."
  (let ((originals (mapcar (lambda (v) (list (gensym) v)) vars)))
    `(let ,(mapcar (lambda (o) `(,(car o) (symbol-value ',(cadr o)))) originals)
       (unwind-protect
           (progn ,@body)
         ,@(mapcar (lambda (o) `(set ',(cadr o) ,(car o))) originals)))))

(defun lxkl-org-share--export (dir)
  "Call `org-html-export-to-html' after establishing bindings based
on config dir."
  (with-values-restored
   (user-mail-address org-html-head org-html-postamble-format)
   (setq user-mail-address (lxkl-org-share--config-var dir 'user-mail-address))
   (setq org-html-head (lxkl-org-share--config-var dir 'org-html-head))
   (setq org-html-postamble-format (lxkl-org-share--config-alist dir "org-html-postamble-format.*" 'org-html-postamble-format))
   (org-html-export-to-html nil t)))

(defun lxkl-org-share/share ()
  "Move upward until a section with the `:share:' tag is
found. Export this section to HTML using Org's export feature,
according to properties and while providing variable bindings as
per config dir. Write `.require' file according to
properties. Finally update Htaccess file. See
https://gitlab.com/lxkl/org-share for comprehensive
documentation."
  (interactive)
  (save-excursion
    (while (and (< 1 (org-outline-level)) (not (member "share" (org-get-tags nil t))))
      (outline-up-heading 1 t))
    (let ((dir (lxkl-org-share--property "Dir"))
          (name (lxkl-org-share--property "Name"))
          (req (lxkl-org-share--property "Require" "all denied")))
      (cl-flet ((filename% (ext) (format "%s/%s.%s" dir name ext)))
        (cond
         ((not (and dir name)) (message "Error: ShareDir or ShareName undefined."))
         ((not (file-exists-p dir)) (message (format "Error: Directory ShareDir (%s) does not exist." dir)))
         (t
          (rename-file (lxkl-org-share--export dir) (filename% "html") t)
          (with-temp-file (filename% "require") (insert req))
          (lxkl-org-share--update-htaccess dir)
          (message (format "Shared to %s." (filename% "html")))))))))

(defun lxkl-org-share/share-all ()
  "Call `lxkl-org-share/share' on all sections having the `:share:'
tag."
  (interactive)
  (org-map-entries (lambda () (when (member "share" (org-get-tags nil t)) (lxkl-org-share/share)))))

(defun lxkl-org-share--new-reference (references)
  "Return new reference by simply counting up."
  (let ((references-numbers (cl-remove-if (lambda (x) (not (numberp x))) (mapcar 'cdr references))))
    (if references-numbers (+ 1 (apply 'max references-numbers)) 0)))

(advice-add 'org-export-new-reference :override #'lxkl-org-share--new-reference)

(provide 'lxkl-org-share)
